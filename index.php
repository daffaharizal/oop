<?php
/*
    Class Animal
 
    Terdapat sebuah class Animal yang memiliki sebuah constructor name, default property legs = 4 dan cold_blooded = no.
*/

// instance
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

echo "<br>";

    /* 
    Output akhir

    Name: Shaun
    Legs: 4
    Cold Blooded: No

    Name: Buduk
    Legs: 4
    Cold Blooded: No
    Jump: "hop hop"

    Name: Kera Sakti
    Legs: 2
    Cold Blooded: No
    Yell: "Auooo"
*/

$sheep = new Animal("Shaun");
echo "Name: " . $sheep->get_name() . "<br>";
echo "Legs: " . $sheep->get_legs();
echo "Cold Blooded: " . $sheep->get_cold_blooded();

echo "<br><br>";

$buduk = new Frog("Buduk");
echo "Name: " . $buduk->get_name() . "<br>";
echo "Legs: " . $buduk->get_legs();
echo "Cold Blooded: " . $buduk->get_cold_blooded();
echo "Jump: ";
$buduk->jump();

echo "<br><br>";

$keraSakti = new Ape("Kera Sakti");
echo "Name: " . $keraSakti->get_name() . "<br>";
echo "Legs: " . $keraSakti->get_legs();
echo "Cold Blooded: " . $keraSakti->get_cold_blooded();
echo "Yell: ";
$keraSakti->yell();
